﻿using UnityEngine;
using System.Collections;
	

public class LineasCuerpo : MonoBehaviour {
	private LineRenderer ManoDCodoD;
	public ZigSkeleton Skel;
	public Material Mat;
	// Use this for initialization
	void Start () {
		ManoDCodoD.material = Mat;
		ManoDCodoD.SetColors(Color.red,Color.red);
		ManoDCodoD.useWorldSpace = true;
	}
	
	// Update is called once per frame
	void Update () {
		ManoDCodoD.SetPosition(1,Skel.RightHand.transform.position);
		ManoDCodoD.SetPosition(2,Skel.RightElbow.transform.position);
	}
}
