﻿using UnityEngine;
using System.Collections;

public class Huesos : MonoBehaviour {
	
	public Transform Ini;
	public Transform End;
	public float SizeIni;
	public float SizeEnd;
	public Material Mat;
	
	private LineRenderer hueso;
	
	// Use this for initialization
	void Start () {
		hueso = gameObject.AddComponent<LineRenderer>();
		hueso.material= Mat;
		hueso.SetColors(Color.blue,Color.white);
		hueso.SetWidth(SizeIni,SizeEnd);
		hueso.SetPosition(0, Ini.transform.position);
		hueso.SetPosition(1,End.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
		hueso.SetPosition(0, Ini.transform.position);
		hueso.SetPosition(1,End.transform.position);
	}
}
