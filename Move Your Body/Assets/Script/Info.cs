﻿using UnityEngine;
using System.Collections;

public class Info : MonoBehaviour {
	
	public GUISkin MySkin;
	public Texture Image;

	void OnGUI(){
		GUI.skin=MySkin;
		
		GUILayout.BeginArea(new Rect(Screen.width/4, Screen.height/4, 1000,1000));
		
		GUI.Label(new Rect(20,40,600,40),"Aplicacion desarrollada por Francisco M Urea Galan");
		GUI.Label(new Rect(20,80,600,40),"Version: 1.2.0");
		GUI.Label(new Rect(20,120,600,40),"Licencia: Creative Commons");
		GUI.Label(new Rect(20,160,600,40),"Aviso Legal:");
		GUI.Label(new Rect(20,200,600,120),"Reconocimiento – NoComercial – CompartirIgual (by-nc-sa): No se permite un uso " +
			"comercial de la obra original ni de las posibles obras derivadas, la distribución de las " +
			"cuales se debe hacer con una licencia igual a la que regula la obra original.");
		GUI.DrawTexture(new Rect(200,320,200,70),Image);
		
		
		GUILayout.EndArea();
		
		if(GUI.Button(new Rect(Screen.width-210,Screen.height-70, 200, 60),"Salir")){
				AutoFade.LoadLevel(0,3,1,Color.white);
		}
		
	}
}
