﻿using UnityEngine;
using System.Collections;

public class SkeletonMiniature : MonoBehaviour {
	
	public Transform Head;
    public Transform Torso;
    public Transform Waist;

    public Transform LeftShoulder;
    public Transform LeftElbow;
    public Transform LeftHand;

    public Transform RightShoulder;
    public Transform RightElbow;
    public Transform RightHand;

    public Transform LeftHip;
    public Transform LeftKnee;
    public Transform LeftFoot;

    public Transform RightHip;
    public Transform RightKnee;
    public Transform RightFoot;
	
	public SkeletonMiniatureFrontal MiniatureFrontal;
	private bool Mostrar;
	private BodyPositions Body;
	
	private Vector3 PosHead;
    private Vector3 PosTorso;
    private Vector3 PosWaist;

    private Vector3 PosLeftShoulder;
    private Vector3 PosLeftElbow;
    private Vector3 PosLeftHand;

    private Vector3 PosRightShoulder;
    private Vector3 PosRightElbow;
    private Vector3 PosRightHand;

    private Vector3 PosLeftHip;
    private Vector3 PosLeftKnee;
    private Vector3 PosLeftFoot;

    private Vector3 PosRightHip;
    private Vector3 PosRightKnee;
    private Vector3 PosRightFoot;
	private int position;
	private int retardo;
	private Vector3 Vaux; 
	
	// Use this for initialization
	void Awake() {
		Mostrar=false;
		Body=null;
		position=0;
		GetInitPosition();
		retardo=0;
	}
	
	// Update is called once per frame
	void Update () {
		if(Mostrar){
			if(retardo==0){
				if(position<Body.Captures){
					switch (Body.Mode){
					case 1:
						LeftHand.transform.position=Torso.position+Body.Hand_Left[position];
						LeftElbow.transform.position=Torso.position+Body.Elbow_Left[position];
						position++;
						break;
					case 2:
						RightHand.transform.position=Torso.position+Body.Hand_Right[position];
						RightElbow.transform.position=Torso.position+Body.Elbow_Right[position];
						position++;
						break;
					case 3:
						LeftHand.transform.position=Torso.position+Body.Hand_Left[position];
						LeftElbow.transform.position=Torso.position+Body.Elbow_Left[position];
						RightHand.transform.position=Torso.position+Body.Hand_Right[position];
						RightElbow.transform.position=Torso.position+Body.Elbow_Right[position];
						position++;
						break;
					case 4:
						Torso.transform.position=Waist.position+Body.Torso[position];
						LeftHand.transform.position=Waist.position+Body.Hand_Left[position];
						LeftElbow.transform.position=Waist.position+Body.Elbow_Left[position];
						RightHand.transform.position=Waist.position+Body.Hand_Right[position];
						RightElbow.transform.position=Waist.position+Body.Elbow_Right[position];
						position++;
						break;
					case 5:
						LeftKnee.transform.position=Torso.transform.position+Body.Knee_Left[position];
						LeftFoot.transform.position=Torso.transform.position+Body.Foot_Left[position];
						position++;
						break;
					case 6:
						RightKnee.transform.position=Torso.transform.position+Body.Knee_Right[position];
						RightFoot.transform.position=Torso.transform.position+Body.Foot_Right[position];
						position++;
						break;
					case 7:
						LeftKnee.transform.position=Torso.transform.position+Body.Knee_Left[position];
						LeftFoot.transform.position=Torso.transform.position+Body.Foot_Left[position];
						RightKnee.transform.position=Torso.transform.position+Body.Knee_Right[position];
						RightFoot.transform.position=Torso.transform.position+Body.Foot_Right[position];
						position++;
						break;				
					}
					retardo=5;
				}else{
					position=0;
					PositionInitial();
					WaitTime();
				}
			}else{
				retardo--;
			}
		}
	}
	
	private void WaitTime(){
		Mostrar=false;
		StartCoroutine(Wait(1.0f));
	}
	
	private IEnumerator Wait(float second){
		yield return new WaitForSeconds(second);
		MiniatureFrontal.Activate();
	}
	
	public void Activate(){
		Mostrar=true;
	}
	
	public void PositionInitial(){
		Head.transform.position= PosHead;
		Torso.transform.position= PosTorso;
		Waist.transform.position= PosWaist;
		
		LeftShoulder.transform.position= PosLeftShoulder;
		LeftElbow.transform.position= PosLeftElbow;
		LeftHand.transform.position= PosLeftHand;
		
		RightShoulder.transform.position= PosRightShoulder;
		RightElbow.transform.position= PosRightElbow;
		RightHand.transform.position= PosRightHand;
		
		LeftHip.transform.position= PosLeftHip;
		LeftKnee.transform.position= PosLeftKnee;
		LeftFoot.transform.position= PosLeftFoot;
		
		RightHip.transform.position= PosRightHip;
		RightKnee.transform.position= PosRightKnee;
		RightFoot.transform.position= PosRightFoot;
	}
	
	private void GetInitPosition(){
		PosHead=Head.transform.position;
		PosTorso=Torso.transform.position;
		PosWaist=Waist.transform.position;
		
		PosLeftShoulder=LeftShoulder.transform.position;
		PosLeftElbow=LeftElbow.transform.position;
		PosLeftHand=LeftHand.transform.position;
		
		PosRightShoulder=RightShoulder.transform.position;
		PosRightElbow=RightElbow.transform.position;
		PosRightHand=RightHand.transform.position;
		
		PosLeftHip=LeftHip.transform.position;
		PosLeftKnee=LeftKnee.transform.position;
		PosLeftFoot=LeftFoot.transform.position;
		
		PosRightHip=RightHip.transform.position;
		PosRightKnee=RightKnee.transform.position;
		PosRightFoot=RightFoot.transform.position;
	}
	
	public void inicializate(BodyPositions bodyIn){
		Body=bodyIn;
		MiniatureFrontal.inicializate(Body);
		Mostrar=true;
	}
}
