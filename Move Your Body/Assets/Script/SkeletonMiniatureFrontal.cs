﻿using UnityEngine;
using System.Collections;

public class SkeletonMiniatureFrontal : MonoBehaviour {
	
	public Transform Head;
    public Transform Torso;
    public Transform Waist;

    public Transform LeftShoulder;
    public Transform LeftElbow;
    public Transform LeftHand;

    public Transform RightShoulder;
    public Transform RightElbow;
    public Transform RightHand;

    public Transform LeftHip;
    public Transform LeftKnee;
    public Transform LeftFoot;

    public Transform RightHip;
    public Transform RightKnee;
    public Transform RightFoot;
	public SkeletonMiniature Miniature;
	
	private bool Mostrar;
	private BodyPositions Body;
	
	private Vector3 PosHead;
    private Vector3 PosTorso;
    private Vector3 PosWaist;

    private Vector3 PosLeftShoulder;
    private Vector3 PosLeftElbow;
    private Vector3 PosLeftHand;

    private Vector3 PosRightShoulder;
    private Vector3 PosRightElbow;
    private Vector3 PosRightHand;

    private Vector3 PosLeftHip;
    private Vector3 PosLeftKnee;
    private Vector3 PosLeftFoot;

    private Vector3 PosRightHip;
    private Vector3 PosRightKnee;
    private Vector3 PosRightFoot;
	private int position;
	private int retardo;
	private Vector3 Vaux; 
	
	// Use this for initialization
	void Awake() {
		Mostrar=false;
		Body=null;
		position=0;
		GetInitPosition();
		retardo=0;
		Vaux=new Vector3(0f,0f,0f);
	}
	
	// Update is called once per frame
	void Update () {
		if(Mostrar){
			if(retardo==0){
				if(position<Body.Captures){
					switch (Body.Mode){
					case 1:
						Vaux.x=Torso.position.x-Body.Hand_Left[position].x;
						Vaux.z=Torso.position.z-Body.Hand_Left[position].z;
						Vaux.y=Torso.position.y+Body.Hand_Left[position].y;
						LeftHand.transform.position=Vaux;
						Vaux.x=Torso.position.x-Body.Elbow_Left[position].x;
						Vaux.z=Torso.position.z-Body.Elbow_Left[position].z;
						Vaux.y=Torso.position.y+Body.Elbow_Left[position].y;
						LeftElbow.transform.position=Vaux;
						position++;
						break;
					case 2:
						Vaux.x=Torso.position.x-Body.Hand_Right[position].x;
						Vaux.z=Torso.position.z-Body.Hand_Right[position].z;
						Vaux.y=Torso.position.y+Body.Hand_Right[position].y;
						RightHand.transform.position=Vaux;
						Vaux.x=Torso.position.x-Body.Elbow_Right[position].x;
						Vaux.z=Torso.position.z-Body.Elbow_Right[position].z;
						Vaux.y=Torso.position.y+Body.Elbow_Right[position].y;
						RightElbow.transform.position=Vaux;
						position++;
						break;
					case 3:
						Vaux.x=Torso.position.x-Body.Hand_Left[position].x;
						Vaux.z=Torso.position.z-Body.Hand_Left[position].z;
						Vaux.y=Torso.position.y+Body.Hand_Left[position].y;
						LeftHand.transform.position=Vaux;
						Vaux.x=Torso.position.x-Body.Elbow_Left[position].x;
						Vaux.z=Torso.position.z-Body.Elbow_Left[position].z;
						Vaux.y=Torso.position.y+Body.Elbow_Left[position].y;
						LeftElbow.transform.position=Vaux;
						
						Vaux.x=Torso.position.x-Body.Hand_Right[position].x;
						Vaux.z=Torso.position.z-Body.Hand_Right[position].z;
						Vaux.y=Torso.position.y+Body.Hand_Right[position].y;
						RightHand.transform.position=Vaux;
						Vaux.x=Torso.position.x-Body.Elbow_Right[position].x;
						Vaux.z=Torso.position.z-Body.Elbow_Right[position].z;
						Vaux.y=Torso.position.y+Body.Elbow_Right[position].y;
						RightElbow.transform.position=Vaux;
						position++;
						break;
					case 4:
						
						Vaux.x=Waist.position.x-Body.Torso[position].x;
						Vaux.z=Waist.position.z-Body.Torso[position].z;
						Vaux.y=Waist.position.y+Body.Torso[position].y;
						Torso.transform.position=Vaux;
						
						Vaux.x=Waist.position.x-Body.Hand_Left[position].x;
						Vaux.z=Waist.position.z-Body.Hand_Left[position].z;
						Vaux.y=Waist.position.y+Body.Hand_Left[position].y;
						LeftHand.transform.position=Vaux;
						Vaux.x=Waist.position.x-Body.Elbow_Left[position].x;
						Vaux.z=Waist.position.z-Body.Elbow_Left[position].z;
						Vaux.y=Waist.position.y+Body.Elbow_Left[position].y;
						LeftElbow.transform.position=Vaux;
						
						Vaux.x=Waist.position.x-Body.Hand_Right[position].x;
						Vaux.z=Waist.position.z-Body.Hand_Right[position].z;
						Vaux.y=Waist.position.y+Body.Hand_Right[position].y;
						RightHand.transform.position=Vaux;
						Vaux.x=Waist.position.x-Body.Elbow_Right[position].x;
						Vaux.z=Waist.position.z-Body.Elbow_Right[position].z;
						Vaux.y=Waist.position.y+Body.Elbow_Right[position].y;
						RightElbow.transform.position=Vaux;
						position++;
						break;
					case 5:
						Vaux.x=Torso.position.x-Body.Knee_Left[position].x;
						Vaux.z=Torso.position.z-Body.Knee_Left[position].z;
						Vaux.y=Torso.position.y+Body.Knee_Left[position].y;
						LeftKnee.transform.position=Vaux;
						Vaux.x=Torso.position.x-Body.Foot_Left[position].x;
						Vaux.z=Torso.position.z-Body.Foot_Left[position].z;
						Vaux.y=Torso.position.y+Body.Foot_Left[position].y;
						LeftFoot.transform.position=Vaux;
						position++;
						break;
					case 6:
						Vaux.x=Torso.position.x-Body.Knee_Right[position].x;
						Vaux.z=Torso.position.z-Body.Knee_Right[position].z;
						Vaux.y=Torso.position.y+Body.Knee_Right[position].y;
						RightKnee.transform.position=Vaux;
						Vaux.x=Torso.position.x-Body.Foot_Right[position].x;
						Vaux.z=Torso.position.z-Body.Foot_Right[position].z;
						Vaux.y=Torso.position.y+Body.Foot_Right[position].y;
						RightFoot.transform.position=Vaux;
						position++;
						break;
					case 7:
						Vaux.x=Torso.position.x-Body.Knee_Left[position].x;
						Vaux.z=Torso.position.z-Body.Knee_Left[position].z;
						Vaux.y=Torso.position.y+Body.Knee_Left[position].y;
						LeftKnee.transform.position=Vaux;
						Vaux.x=Torso.position.x-Body.Foot_Left[position].x;
						Vaux.z=Torso.position.z-Body.Foot_Left[position].z;
						Vaux.y=Torso.position.y+Body.Foot_Left[position].y;
						LeftFoot.transform.position=Vaux;
						
						Vaux.x=Torso.position.x-Body.Knee_Right[position].x;
						Vaux.z=Torso.position.z-Body.Knee_Right[position].z;
						Vaux.y=Torso.position.y+Body.Knee_Right[position].y;
						RightKnee.transform.position=Vaux;
						Vaux.x=Torso.position.x-Body.Foot_Right[position].x;
						Vaux.z=Torso.position.z-Body.Foot_Right[position].z;
						Vaux.y=Torso.position.y+Body.Foot_Right[position].y;
						RightFoot.transform.position=Vaux;
						position++;
						break;				
					}
					retardo=5;
				}else{
					position=0;
					PositionInitial();
					WaitTime();
				}
			}else{
				retardo--;
			}
		}
	}
	
	private void WaitTime(){
		Mostrar=false;
		StartCoroutine(Wait(5.0f));
	}
	
	private IEnumerator Wait(float second){
		yield return new WaitForSeconds(second);
		Miniature.Activate();
	}
	
	public void Activate(){
		Mostrar=true;
	}
	
	public void PositionInitial(){
		Head.transform.position= PosHead;
		Torso.transform.position= PosTorso;
		Waist.transform.position= PosWaist;
		
		LeftShoulder.transform.position= PosLeftShoulder;
		LeftElbow.transform.position= PosLeftElbow;
		LeftHand.transform.position= PosLeftHand;
		
		RightShoulder.transform.position= PosRightShoulder;
		RightElbow.transform.position= PosRightElbow;
		RightHand.transform.position= PosRightHand;
		
		LeftHip.transform.position= PosLeftHip;
		LeftKnee.transform.position= PosLeftKnee;
		LeftFoot.transform.position= PosLeftFoot;
		
		RightHip.transform.position= PosRightHip;
		RightKnee.transform.position= PosRightKnee;
		RightFoot.transform.position= PosRightFoot;
	}
	
	private void GetInitPosition(){
		PosHead=Head.transform.position;
		PosTorso=Torso.transform.position;
		PosWaist=Waist.transform.position;
		
		PosLeftShoulder=LeftShoulder.transform.position;
		PosLeftElbow=LeftElbow.transform.position;
		PosLeftHand=LeftHand.transform.position;
		
		PosRightShoulder=RightShoulder.transform.position;
		PosRightElbow=RightElbow.transform.position;
		PosRightHand=RightHand.transform.position;
		
		PosLeftHip=LeftHip.transform.position;
		PosLeftKnee=LeftKnee.transform.position;
		PosLeftFoot=LeftFoot.transform.position;
		
		PosRightHip=RightHip.transform.position;
		PosRightKnee=RightKnee.transform.position;
		PosRightFoot=RightFoot.transform.position;
	}
	
	public void inicializate(BodyPositions bodyIn){
		Body=bodyIn;
		Mostrar=false;
	}
}
