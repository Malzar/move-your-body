﻿using UnityEngine;
using System.Collections;

public class MotionCapture : MonoBehaviour {
	
	const int DELAY=30;
	
	protected FileBrowser m_fileBrowser;
	protected string VideoPath;
	
	public GUISkin MySkin;
	public SerialGame Serializator;
	public ZigSkeleton Skel;
	public BodyPositions Body;
	public Texture2D m_directoryImage,m_fileImage;
	public bool Calibrated;
	public bool StartCapture;
	public int DelayRecord;
	
	private bool SaveFile;
	private bool End;
	private bool StartRecord;
	private string FileName="";
	private int Mode;
	private string StrNumRep;
	private int NumRep;
	
	void Awake(){
		Serializator = new SerialGame();
		Body = new BodyPositions();
		
		Calibrated =false;
		StartCapture =false;
		SaveFile=false;
		DelayRecord=0;
		Mode=0;
		StrNumRep="";
		StartRecord=false;
		End=false;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//comprobar la calibracion
		if(StartRecord){
			Body.VideoUrl=VideoPath;
			Body.Repetitions=int.Parse(StrNumRep);
			if(Calibrated){
				if((Input.GetKeyDown(KeyCode.Home))||(Input.GetKeyDown(KeyCode.Space))){
					StartCapture=true;
					GameObject.Find("Float Text").guiText.text="Grabando movimientos";
					GameObject.Find("Float Text").guiText.enabled=true;
				}
				
				if(StartCapture){
					if(DelayRecord==0){
						SaveSkeletonPosition(Mode);
						DelayRecord=DELAY;
					}
					DelayRecord--;
				}
				
				if((Input.GetKeyDown(KeyCode.Escape)==true)||(Input.GetKeyDown(KeyCode.End)==true)){
					StartCapture=false;
					SaveFile=true;
				}
			}else{
				GameObject.Find("Float Text").guiText.text = "Calibrando su cuerpo";
				Calibrated = IsCalibrated();
			}
		}
	}
	
	void OnGUI(){
		if(!StartRecord){
			if (m_fileBrowser != null) {
				m_fileBrowser.OnGUI();
			} else {
				GUILayout.BeginArea(new Rect(Screen.width/4, Screen.height/4, 800,800));
				GUI.Box(new Rect(0,0,400,400),"Creacion de ejercicio");
				GUI.Label(new Rect(20,40,400,20),"Nombre del fichero (sin espacios en blanco)");
				FileName = GUI.TextField(new Rect(20, 60, 200, 20), FileName, 50);
				GUI.Label(new Rect(20,90,400,20),"Selecciones un video (recomendado)");
				
				OnGUIMain();
				
				GUI.Label(new Rect(20,150,400,20),"Seleccione un tipo de ejercicio");
				if(GUI.Button(new Rect(20,180, 20, 20),"1")){
					Mode=1;
				}
				GUI.Label(new Rect(50,180,150,20),"Brazo izquierdo");
				if(GUI.Button(new Rect(20,210, 20, 20),"2")){
					Mode=2;
				}
				GUI.Label(new Rect(50,210,150,20),"Brazo derecho");
				if(GUI.Button(new Rect(20,240, 20, 20),"3")){
					Mode=3;
				}
				GUI.Label(new Rect(50,240,150,20),"Ambos brazos");
				if(GUI.Button(new Rect(20,270, 20, 20),"4")){
					Mode=4;
				}
				GUI.Label(new Rect(50,270,150,20),"Parte superior cuerpo");
				if(GUI.Button(new Rect(210,180, 20, 20),"5")){
					Mode=5;
				}
				GUI.Label(new Rect(240,180,100,20),"Pierna izquierda");
				if(GUI.Button(new Rect(210,210, 20, 20),"6")){
					Mode=6;
				}
				GUI.Label(new Rect(240,210,100,20),"Pierna derecha");
				if(GUI.Button(new Rect(210,240, 20, 20),"7")){
					Mode=7;
				}
				GUI.Label(new Rect(240,240,100,20),"Ambas piernas");
				
				GUI.Label(new Rect(20,300,300,20),"Indique el numero de repeticiones del ejercicio:");
				StrNumRep= GUI.TextField(new Rect(20, 320, 40, 20), StrNumRep, 2);
				
				if((Mode==0)||(FileName.Equals("")||(StrNumRep.Equals("")))){
					GUI.Label(new Rect(20,340,350,60),"Debe seleccionar al menos un tipo de ejercicio, escribir un nombre e indicar el numero de repeticiones");
				}else{
					if(FileName.Contains(" ")){
						GUI.Label(new Rect(20,340,300,60),"El nombre del ejercicio no debe contener espacios en blanco");
					}else{
						if(GUI.Button(new Rect(100,320,250,60),"Crear")) StartRecord=true;
					}
				}
				GUILayout.EndArea();
			}
		}
		
		if(SaveFile){
			GameObject.Find("Float Text").guiText.enabled=false;
			GUI.skin=MySkin;
			if(GUI.Button(new Rect(Screen.width/2 -340,Screen.height/2, 200, 60), "Guardar")){
				SaveExercise(FileName);
				GameObject.Find("Float Text").guiText.text="Su fichero ha sido generado";
				GameObject.Find("Float Text").guiText.enabled=true;
				End=true;
				SaveFile=false;
			}
			if(GUI.Button(new Rect(Screen.width/2 -100,Screen.height/2, 200, 60),"Repetir")){
				Body = new BodyPositions();
				StartRecord = true;
				SaveFile=false;
			}
			if(GUI.Button(new Rect(Screen.width/2 + 140,Screen.height/2, 200, 60),"Borrar")){
				End=true;
				SaveFile=false;
			}
			GUI.skin=null;
		}
		if(End){
			GUI.skin=MySkin;
			if(GUI.Button(new Rect(Screen.width/2 -220,Screen.height/2, 200, 60), "Salir")){
				AutoFade.LoadLevel(0,3,1,Color.white);
			}
			if(GUI.Button(new Rect(Screen.width/2 +20,Screen.height/2, 230, 60), "Grabar otro")){
				Body = new BodyPositions();
				Serializator = new SerialGame();
				StartRecord = false;
				End=false;
				FileName="";
				StrNumRep="";
				Mode=0;
				GameObject.Find("Float Text").guiText.enabled=false;
			}
			GUI.skin=null;
		}
	}
	
	protected void OnGUIMain() {
 		GUILayout.BeginArea(new Rect(-120, 110, 400,20));
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		//m_textPath=Application.dataPath;
		//GUILayout.Label(VideoPath ?? "none selected");
		if (GUILayout.Button("Mp4", GUILayout.ExpandWidth(false))) {
			m_fileBrowser = new FileBrowser(new Rect(0, 100, 600, 500),"Elija el video",FileSelectedCallback);
			m_fileBrowser.SelectionPattern= "*.mp4";
			m_fileBrowser.DirectoryImage = m_directoryImage;
			m_fileBrowser.FileImage = m_fileImage;
		}
		if (GUILayout.Button("Avi", GUILayout.ExpandWidth(false))) {
			m_fileBrowser = new FileBrowser(new Rect(100, 100, 600, 500),"Elija el video",FileSelectedCallback);
			m_fileBrowser.SelectionPattern = "*.avi";
			m_fileBrowser.DirectoryImage = m_directoryImage;
			m_fileBrowser.FileImage = m_fileImage;
		}
		if (GUILayout.Button("Mpeg", GUILayout.ExpandWidth(false))) {
			m_fileBrowser = new FileBrowser(new Rect(100, 100, 600, 500),"Elija el video",FileSelectedCallback);
			m_fileBrowser.SelectionPattern = "*.mpeg";
			m_fileBrowser.DirectoryImage = m_directoryImage;
			m_fileBrowser.FileImage = m_fileImage;
		}
		if (GUILayout.Button("MOV", GUILayout.ExpandWidth(false))) {
			m_fileBrowser = new FileBrowser(new Rect(100, 100, 600, 500),"Elija el video",FileSelectedCallback);
			m_fileBrowser.SelectionPattern = "*.mov";
			m_fileBrowser.DirectoryImage = m_directoryImage;
			m_fileBrowser.FileImage = m_fileImage;
		}
		if (GUILayout.Button("Otro formato", GUILayout.ExpandWidth(false))) {
			m_fileBrowser = new FileBrowser(new Rect(100, 100, 600, 500),"Elija el video",FileSelectedCallback);
			m_fileBrowser.SelectionPattern = "*.*";
			m_fileBrowser.DirectoryImage = m_directoryImage;
			m_fileBrowser.FileImage = m_fileImage;
		}	
		
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
	}
	
	protected void FileSelectedCallback(string path) {
		m_fileBrowser = null;
		VideoPath = path;
	}
	
	public bool IsCalibrated(){
		if(Skel.LeftKnee.transform.position!=new Vector3(0f,1.945635f,0f)){
			GameObject.Find("Float Text").guiText.enabled=false;
			return true;
		}else{
			return false;
		}
	}
	
	public void SaveExercise(string fichName){
		Serializator.Save(Body, fichName);
	}
	
	public bool SaveSkeletonPosition(int mod){
		try{	
			Body.SaveSqueleton(mod,Skel);
			return true;
		}catch{
			return false;
		}
	}
}
