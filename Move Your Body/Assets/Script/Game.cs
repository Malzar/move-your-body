﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System;
using System.IO;
using System.Collections.Generic;


public class Game : MonoBehaviour {
	
	const int DELAY=60;
	
	protected FileBrowser m_fileBrowser;
	
	public SerialGame Serializator;
	public ZigSkeleton Skel;
	public BodyPositions Body;
	public Material MaterialBubble;
	public GameObject prefab;
	public GUISkin MySkin;
	public Texture2D m_directoryImage,m_fileImage;
	public SkeletonMiniature Miniatura;
	
	private Bubbles BubbleInput;
	
	private float DistanceError;
	private int CurrentCapture;
	private int CureentRepetition;
	private int Mod;
	private int NumberCaptrues;
	private int NumberPosition;
	private int Repetitions;
	private bool EndCaptures;
	private bool Init;
	private bool Cargado;
	private bool StartGame;
	private bool MenuButtons;
	private bool MenuButtons2;
	private bool Dificult;
	private bool Multi;
	private string FileName;
	private int points;
	private int extrapoints;
	private int wrong;
	
	
	
	// Use this for initialization
	void Awake () {
		Serializator = new SerialGame();
		Body = new BodyPositions();
		BubbleInput = new Bubbles();
		
		points=0;		
		DistanceError=0.2f;
		CurrentCapture=1;
		CureentRepetition=0;
		Init=false;
		Cargado=false;
		StartGame=false;
		MenuButtons=false;
		MenuButtons2=false;
		Dificult=false;
		Multi=false;
		FileName="";
		GameObject.Find("Points").guiText.text="";
	}
	
	void Start(){		
	}
	
	// Update is called once per frame
	void Update () {
		if(Cargado){
			if(Dificult){
				if(!StartGame){
					Body=Serializator.Load(FileName);
					Repetitions = Body.Repetitions;
					NumberCaptrues=Body.Captures;
					Mod = Body.Mode;
					BubbleInput.LoadBubble(Body);
					BubbleInput.InitBubbles(DistanceError+0.1f,Mod,MaterialBubble);
					LoadVideo(Body.VideoUrl);
					wrong=0;
					extrapoints=100/NumberCaptrues;
					Multi=false;
					StartGame=true;
					Miniatura.inicializate(Body);					
				}else{
					if(IsCalibrated()){
						GameObject.Find("Points").guiText.text=points.ToString();
						if(Input.GetKeyDown(KeyCode.Home)||Input.GetKeyDown(KeyCode.Space)){
							Init=true;
						}
						//slato de posicion
						if(Input.GetKeyDown(KeyCode.Escape)){
							CurrentCapture++;
							if(CurrentCapture==NumberCaptrues){
								CureentRepetition++;
								CurrentCapture=1;
							}
							wrong++;
							points=points-(wrong*20);
							if(points<0) points=0;
						}
						//activar menu
						if(Input.GetKeyDown(KeyCode.End)){
							MenuButtons=true;
						}
						if(Init){
							if(CureentRepetition<Repetitions){
								
								switch (Mod){
								case 1:
									BubbleInput.PutBubbles(CurrentCapture,Mod,Skel.Torso.transform.position);
									if(BubbleInput.PositionComparator(DistanceError, Skel.LeftElbow.transform.position, Skel.LeftHand.transform.position)){
										//instanciamos las particulas para que parezca que explotan
										Instantiate(prefab, Skel.LeftHand.transform.position,Quaternion.identity);
										Instantiate(prefab, Skel.LeftElbow.transform.position,Quaternion.identity);
										
										points=points+25+extrapoints;
										CurrentCapture++;
										if(CurrentCapture==NumberCaptrues){
											CureentRepetition++;
											CurrentCapture=1;
										}
									}
									break;
								case 2:
									BubbleInput.PutBubbles(CurrentCapture,Mod,Skel.Torso.transform.position);
									if(BubbleInput.PositionComparator(DistanceError, Skel.RightElbow.transform.position, Skel.RightHand.transform.position)){
										//instanciamos las particulas para que parezca que explotan
										Instantiate(prefab, Skel.RightHand.transform.position,Quaternion.identity);
										Instantiate(prefab, Skel.RightElbow.transform.position,Quaternion.identity);
										
										points=points+25+extrapoints;
										CurrentCapture++;
										if(CurrentCapture==NumberCaptrues){
											CureentRepetition++;
											CurrentCapture=1;
										}
									}
									break;
								case 3:
									BubbleInput.PutBubbles(CurrentCapture,Mod,Skel.Torso.transform.position);
									if(BubbleInput.PositionComparator(DistanceError, Skel.LeftElbow.transform.position, Skel.LeftHand.transform.position,Skel.RightElbow.transform.position, Skel.RightHand.transform.position)){
										//instanciamos las particulas para que parezca que explotan
										Instantiate(prefab, Skel.LeftKnee.transform.position,Quaternion.identity);
										Instantiate(prefab, Skel.LeftFoot.transform.position,Quaternion.identity);
										Instantiate(prefab, Skel.RightHand.transform.position,Quaternion.identity);
										Instantiate(prefab, Skel.RightElbow.transform.position,Quaternion.identity);
										
										points=points+25+extrapoints;
										CurrentCapture++;
										if(CurrentCapture==NumberCaptrues){
											CureentRepetition++;
											CurrentCapture=1;
										}
									}
									break;
								case 4:
									BubbleInput.PutBubbles(CurrentCapture,Mod,Skel.Waist.transform.position);
									if(BubbleInput.PositionComparator(DistanceError, Skel.Torso.transform.position, Skel.LeftElbow.transform.position, Skel.LeftHand.transform.position,Skel.RightElbow.transform.position, Skel.RightHand.transform.position)){
										//instanciamos las particulas para que parezca que explotan
										Instantiate(prefab, Skel.Torso.transform.position,Quaternion.identity);
										Instantiate(prefab, Skel.LeftKnee.transform.position,Quaternion.identity);
										Instantiate(prefab, Skel.LeftFoot.transform.position,Quaternion.identity);
										Instantiate(prefab, Skel.RightHand.transform.position,Quaternion.identity);
										Instantiate(prefab, Skel.RightElbow.transform.position,Quaternion.identity);
										
										points=points+25+extrapoints;
										CurrentCapture++;
										if(CurrentCapture==NumberCaptrues){
											CureentRepetition++;
											CurrentCapture=1;
										}
									}
									
									break;
								case 5:
									BubbleInput.PutBubbles(CurrentCapture,Mod,Skel.Torso.transform.position);
									if(BubbleInput.PositionComparator(DistanceError, Skel.LeftKnee.transform.position, Skel.LeftFoot.transform.position)){
										//instanciamos las particulas para que parezca que explotan
										Instantiate(prefab, Skel.LeftKnee.transform.position,Quaternion.identity);
										Instantiate(prefab, Skel.LeftFoot.transform.position,Quaternion.identity);
										
										points=points+25+extrapoints;
										CurrentCapture++;
										if(CurrentCapture==NumberCaptrues){
											CureentRepetition++;
											CurrentCapture=1;
										}
									}
									break;
								case 6:
									BubbleInput.PutBubbles(CurrentCapture,Mod,Skel.Torso.transform.position);
									if(BubbleInput.PositionComparator(DistanceError, Skel.RightKnee.transform.position, Skel.RightFoot.transform.position)){
										//instanciamos las particulas para que parezca que explotan
										Instantiate(prefab, Skel.RightKnee.transform.position,Quaternion.identity);
										Instantiate(prefab, Skel.RightFoot.transform.position,Quaternion.identity);
										
										points=points+25+extrapoints;
										CurrentCapture++;
										if(CurrentCapture==NumberCaptrues){
											CureentRepetition++;
											CurrentCapture=1;
										}
									}
									break;
								case 7:
									BubbleInput.PutBubbles(CurrentCapture,Mod,Skel.Torso.transform.position);
									if(BubbleInput.PositionComparator(DistanceError, Skel.LeftKnee.transform.position, Skel.LeftFoot.transform.position, Skel.RightKnee.transform.position, Skel.RightFoot.transform.position)){
										//instanciamos las particulas para que parezca que explotan
										Instantiate(prefab, Skel.LeftKnee.transform.position,Quaternion.identity);
										Instantiate(prefab, Skel.LeftFoot.transform.position,Quaternion.identity);
										Instantiate(prefab, Skel.RightKnee.transform.position,Quaternion.identity);
										Instantiate(prefab, Skel.RightFoot.transform.position,Quaternion.identity);
										
										points=points+25+extrapoints;
										CurrentCapture++;
										if(CurrentCapture==NumberCaptrues){
											CureentRepetition++;
											CurrentCapture=1;
										}
									}
									break;
								}
							}else{
								MenuButtons2=true;
								BubbleInput.DestroyBubbles();
								if(!Multi){
									if(DistanceError==0.25f){
										
									}else if(DistanceError==0.20f){
										points=(int)(points*(1.5));
									}else{
										points=points*2;
									}
									Multi=true;
								}
							}
						}
					}
				}
			}
		}
	}
	
	void OnGUI(){
		//cargado de ficheros
		if(!Cargado){			
			if(FileName==""){
				if (m_fileBrowser != null) {
					m_fileBrowser.OnGUI();
				} else {
					GUILayout.BeginArea(new Rect(Screen.width/2-140,Screen.height/2,200,100));
					GUI.skin=MySkin;
					if (GUILayout.Button("Abrir Ejercicio",  GUILayout.Width(280), GUILayout.Height(60))){
						m_fileBrowser = new FileBrowser(new Rect(100, 100, 600, 500),"Elije el video",FileSelectedCallback);
						m_fileBrowser.SelectionPattern= "*.myb";
						m_fileBrowser.DirectoryImage = m_directoryImage;
						m_fileBrowser.FileImage = m_fileImage;
					}
					GUI.skin=null;
					GUILayout.EndArea();
				}
			}else{
				Cargado=true;
				Dificult=false;
			}
		}
		//menu dificultad
		if(!Dificult&&Cargado){
			GUI.skin=MySkin;
			if(GUI.Button(new Rect(Screen.width/2 -340,Screen.height/2, 200, 60), "Facil")){
				DistanceError=0.25f;
				StartGame=false;
				Dificult=true;
			}
			if(GUI.Button(new Rect(Screen.width/2 -100,Screen.height/2, 200, 60),"Normal")){
				DistanceError=0.2f;
				StartGame=false;
				Dificult=true;
			}
			if(GUI.Button(new Rect(Screen.width/2 + 140,Screen.height/2, 200, 60),"Dificil")){
				DistanceError=0.15f;
				StartGame=false;
				Dificult=true;
			}
			GUI.skin=null;
		}
		//menu opciones
		if(MenuButtons){
			GUI.skin=MySkin;
			if(GUI.Button(new Rect(Screen.width/2 -335,Screen.height/2, 200, 80), "Continuar")){
				MenuButtons=false;
			}
			if(GUI.Button(new Rect(Screen.width/2 -100,Screen.height/2, 230, 80),"Otro ejercicio")){
				BubbleInput.DestroyBubbles();
				FileName="";
				Cargado=false;
				CureentRepetition=0;
				MenuButtons=false;
				CurrentCapture=1;
			}
			if(GUI.Button(new Rect(Screen.width/2 + 155,Screen.height/2, 200, 80),"Salir")){
				AutoFade.LoadLevel(0,3,1,Color.white);
			}
			GUI.skin=null;
		}
		//menu repetir
		if(MenuButtons2){
			GUI.skin=MySkin;
			if(GUI.Button(new Rect(Screen.width/2 -250,Screen.height/2, 230, 80),"Otro ejercicio")){
				BubbleInput.DestroyBubbles();
				FileName="";
				Cargado=false;
				CureentRepetition=0;
				MenuButtons2=false;
				CurrentCapture=1;
			}
			if(GUI.Button(new Rect(Screen.width/2 + 20,Screen.height/2, 200, 80),"Salir")){
				AutoFade.LoadLevel(0,3,1,Color.white);
			}
			GUI.skin=null;
		}
	}
	
	public void LoadVideo(string VideoPath){
		ProcessStartInfo procinfo = new ProcessStartInfo();
		procinfo.WorkingDirectory= @"C:\Program Files (x86)\VideoLAN\VLC\"; 
		procinfo.FileName = "vlc.exe";
		procinfo.Arguments =@VideoPath;
		System.Diagnostics.Process.Start(procinfo);
		StartGame=true;
	}
	
	protected void FileSelectedCallback(string path) {
		m_fileBrowser = null;
		FileName = path;
	}
	
	public bool IsCalibrated(){
		if(Skel.Head.transform.position!=new Vector3(0f,1.945635f,0f)){
			return true;
		}else{
			return false;
		}
	}
}
