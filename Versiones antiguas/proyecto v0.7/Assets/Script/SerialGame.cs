﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class SerialGame : MonoBehaviour {
	
	private List<double> Lines = new List<double>();
	private StreamWriter Wr;
	private StreamReader Rd;
	private FileStream FS;
	private BodyPositions Body = new BodyPositions();
	
	public BodyPositions Load(string filename){
		Debug.Log("Archivo "+filename);
		FS = new FileStream(filename, FileMode.Open, FileAccess.Read);
		Rd = new StreamReader(FS);
		float x=0f;
		float y=0f;
		float z=0f;
		string line;
		int change=0;
		int positionList=0;
		
		Body.Mode=int.Parse(Rd.ReadLine());
		Body.Repetitions=int.Parse(Rd.ReadLine());
		Body.Captures=int.Parse(Rd.ReadLine());
		Body.VideoUrl=Rd.ReadLine();
		
		Body.Head = new List<Vector3>();
		Body.Elbow_Left = new List<Vector3>();
		Body.Hand_Left = new List<Vector3>();
		Body.Knee_Left = new List<Vector3>();
		Body.Foot_Left = new List<Vector3>();
		Body.Elbow_Right = new List<Vector3>();
		Body.Hand_Right = new List<Vector3>();
		Body.Knee_Right = new List<Vector3>();
		Body.Foot_Right = new List<Vector3>();
		Body.Torso = new List<Vector3>();
		
		while ((line = Rd.ReadLine()) != null){
			if(change==0){
				x = float.Parse(line);
				change++;
			}else if(change==1){
				y = float.Parse(line);
				change++;
			}else{
				z = float.Parse(line);
				change = 0;
				Vector3 Vaux = new Vector3(x,y,z);
				
				switch (Body.Mode){
				case 1:					
					if(positionList==1){
						Body.Hand_Left.Add(Vaux);
						positionList++;
					}
					if(positionList==0){
						Body.Elbow_Left.Add(Vaux);
						positionList++;
					}
					if(positionList==2) positionList=0;
					break;
				case 2:
					if(positionList==1){
						Body.Hand_Right.Add(Vaux);
						positionList++;
					} 
					if(positionList==0){
						Body.Elbow_Right.Add(Vaux);
						positionList++;
					}
					if(positionList==2) positionList=0;
					break;
				case 3:
					if(positionList==3){
						Body.Hand_Right.Add(Vaux);
						positionList++;
					}
					if(positionList==2){
						Body.Elbow_Right.Add(Vaux);
						positionList++;
					}
					if(positionList==1){
						Body.Hand_Left.Add(Vaux);
						positionList++;
					} 
					if(positionList==0){
						Body.Elbow_Left.Add(Vaux);
						positionList++;
					}
					if(positionList==4) positionList=0;
					break;
				case 4:
					if(positionList==4){
						Body.Hand_Right.Add(Vaux);
						positionList++;
					}
					if(positionList==3){
						Body.Elbow_Right.Add(Vaux);
						positionList++;
					}
					if(positionList==2){
						Body.Hand_Left.Add(Vaux);
						positionList++;
					}
					if(positionList==1){
						Body.Elbow_Left.Add(Vaux);
						positionList++;
					}
					if(positionList==0){
						Body.Torso.Add(Vaux);
						positionList++;
					}
					if(positionList==5) positionList=0;
					break;
				case 5:
					if(positionList==1){
						Body.Foot_Left.Add(Vaux);
						positionList++;
					}
					if(positionList==0){
						Body.Knee_Left.Add(Vaux);
						positionList++;
					}
					if(positionList==2) positionList=0;
					break;
				case 6:
					if(positionList==1){
						Body.Foot_Right.Add(Vaux);
						positionList++;
					}
					if(positionList==0){
						Body.Knee_Right.Add(Vaux);
						positionList++;
					}
					if(positionList==2) positionList=0;
					break;		
				case 7:
					if(positionList==3){
						Body.Foot_Right.Add(Vaux);
						positionList++;
					}
					if(positionList==2){
						Body.Knee_Right.Add(Vaux);
						positionList++;
					}
					if(positionList==1){
						Body.Foot_Left.Add(Vaux);
						positionList++;
					}
					if(positionList==0){
						Body.Knee_Left.Add(Vaux);
						positionList++;
					}					
					if(positionList==4) positionList=0;
					break;
				case 8:
					if(positionList==8){
						Body.Foot_Right.Add(Vaux);
						positionList++;
					}
					if(positionList==7){
						Body.Knee_Right.Add(Vaux);
						positionList++;
					}
					if(positionList==6){
						Body.Foot_Left.Add(Vaux);
						positionList++;
					}
					if(positionList==5){
						Body.Knee_Left.Add(Vaux);
						positionList++;
					}
					if(positionList==4){
						Body.Hand_Right.Add(Vaux);
						positionList++;
					}
					if(positionList==3){
						Body.Elbow_Right.Add(Vaux);
						positionList++;
					}
					if(positionList==2){
						Body.Hand_Left.Add(Vaux);
						positionList++;
					}
					if(positionList==1){
						Body.Elbow_Left.Add(Vaux);
						positionList++;
					}
					if(positionList==0){
						Body.Torso.Add(Vaux);
						positionList++;
					}
					if(positionList==9) positionList=0;
					break;
				}
				
			}
		}
		
		return Body;
		
	}
	
	public void Save(BodyPositions body,string filename){
	
		//lanzar una excepcion de que ya este el fichero
		FS = new FileStream(filename+".myb", FileMode.Create, FileAccess.Write, FileShare.ReadWrite);		
		Wr = new StreamWriter(FS);
		
		Wr.WriteLine(body.Mode);
		Wr.WriteLine(body.Repetitions);
		Wr.WriteLine(body.Captures);
		Wr.WriteLine(body.VideoUrl);
		
		switch (body.Mode){
		case 1:
			for(int i=0; i<=body.Captures-1; i++){
				Lines.Add(body.Elbow_Left[i].x);
				Lines.Add(body.Elbow_Left[i].y);
				Lines.Add(body.Elbow_Left[i].z);
				
				Lines.Add(body.Hand_Left[i].x);
				Lines.Add(body.Hand_Left[i].y);
				Lines.Add(body.Hand_Left[i].z);
			}					
			break;
		case 2:
			for(int i=0; i<=body.Captures-1; i++){
				Lines.Add(body.Elbow_Right[i].x);
				Lines.Add(body.Elbow_Right[i].y);
				Lines.Add(body.Elbow_Right[i].z);
				
				Lines.Add(body.Hand_Right[i].x);
				Lines.Add(body.Hand_Right[i].y);
				Lines.Add(body.Hand_Right[i].z);
			}
			break;
		case 3:
			for(int i=0; i<=body.Captures-1; i++){
				Lines.Add(body.Elbow_Left[i].x);
				Lines.Add(body.Elbow_Left[i].y);
				Lines.Add(body.Elbow_Left[i].z);
				
				Lines.Add(body.Hand_Left[i].x);
				Lines.Add(body.Hand_Left[i].y);
				Lines.Add(body.Hand_Left[i].z);
				
				Lines.Add(body.Elbow_Right[i].x);
				Lines.Add(body.Elbow_Right[i].y);
				Lines.Add(body.Elbow_Right[i].z);
				
				Lines.Add(body.Hand_Right[i].x);
				Lines.Add(body.Hand_Right[i].y);
				Lines.Add(body.Hand_Right[i].z);
			}
			break;
		case 4:
			for(int i=0; i<=body.Captures-1; i++){
				Lines.Add(body.Torso[i].x);
				Lines.Add(body.Torso[i].y);
				Lines.Add(body.Torso[i].z);
				
				Lines.Add(body.Elbow_Left[i].x);
				Lines.Add(body.Elbow_Left[i].y);
				Lines.Add(body.Elbow_Left[i].z);
				
				Lines.Add(body.Hand_Left[i].x);
				Lines.Add(body.Hand_Left[i].y);
				Lines.Add(body.Hand_Left[i].z);
				
				Lines.Add(body.Elbow_Right[i].x);
				Lines.Add(body.Elbow_Right[i].y);
				Lines.Add(body.Elbow_Right[i].z);
				
				Lines.Add(body.Hand_Right[i].x);
				Lines.Add(body.Hand_Right[i].y);
				Lines.Add(body.Hand_Right[i].z);
			}
			break;
		case 5:
			for(int i=0; i<=body.Captures-1; i++){
				Lines.Add(body.Knee_Left[i].x);
				Lines.Add(body.Knee_Left[i].y);
				Lines.Add(body.Knee_Left[i].z);
				
				Lines.Add(body.Foot_Left[i].x);
				Lines.Add(body.Foot_Left[i].y);
				Lines.Add(body.Foot_Left[i].z);
			}	
			break;
		case 6:
			for(int i=0; i<=body.Captures-1; i++){
				Lines.Add(body.Knee_Right[i].x);
				Lines.Add(body.Knee_Right[i].y);
				Lines.Add(body.Knee_Right[i].z);
				
				Lines.Add(body.Foot_Right[i].x);
				Lines.Add(body.Foot_Right[i].y);
				Lines.Add(body.Foot_Right[i].z);
			}
			break;
		case 7:
			for(int i=0; i<=body.Captures-1; i++){
				Lines.Add(body.Knee_Left[i].x);
				Lines.Add(body.Knee_Left[i].y);
				Lines.Add(body.Knee_Left[i].z);
				
				Lines.Add(body.Foot_Left[i].x);
				Lines.Add(body.Foot_Left[i].y);
				Lines.Add(body.Foot_Left[i].z);
				
				Lines.Add(body.Knee_Right[i].x);
				Lines.Add(body.Knee_Right[i].y);
				Lines.Add(body.Knee_Right[i].z);
				
				Lines.Add(body.Foot_Right[i].x);
				Lines.Add(body.Foot_Right[i].y);
				Lines.Add(body.Foot_Right[i].z);
			}	
			break;
		case 8:
			for(int i=0; i<=body.Captures-1; i++){
				Lines.Add(body.Torso[i].x);
				Lines.Add(body.Torso[i].y);
				Lines.Add(body.Torso[i].z);
				
				Lines.Add(body.Elbow_Left[i].x);
				Lines.Add(body.Elbow_Left[i].y);
				Lines.Add(body.Elbow_Left[i].z);
				
				Lines.Add(body.Hand_Left[i].x);
				Lines.Add(body.Hand_Left[i].y);
				Lines.Add(body.Hand_Left[i].z);
				
				Lines.Add(body.Elbow_Right[i].x);
				Lines.Add(body.Elbow_Right[i].y);
				Lines.Add(body.Elbow_Right[i].z);
				
				Lines.Add(body.Hand_Right[i].x);
				Lines.Add(body.Hand_Right[i].y);
				Lines.Add(body.Hand_Right[i].z);
				
				Lines.Add(body.Knee_Left[i].x);
				Lines.Add(body.Knee_Left[i].y);
				Lines.Add(body.Knee_Left[i].z);
				
				Lines.Add(body.Foot_Left[i].x);
				Lines.Add(body.Foot_Left[i].y);
				Lines.Add(body.Foot_Left[i].z);
				
				Lines.Add(body.Knee_Right[i].x);
				Lines.Add(body.Knee_Right[i].y);
				Lines.Add(body.Knee_Right[i].z);
				
				Lines.Add(body.Foot_Right[i].x);
				Lines.Add(body.Foot_Right[i].y);
				Lines.Add(body.Foot_Right[i].z);
			}
			break;
		default:
			//lanzar excepcion
			break;
		}		
		
		for(int i=0; i< Lines.Count; i++){
			Wr.WriteLine(Lines[i].ToString());
		}
		
		Wr.Close();
	}

}
