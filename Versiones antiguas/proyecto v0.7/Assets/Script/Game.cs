﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System;
using System.IO;
using System.Collections.Generic;


public class Game : MonoBehaviour {
	
	const int DELAY=60;
	
	protected FileBrowser m_fileBrowser;
	
	public SerialGame Serializator;
	public ZigSkeleton Skel;
	public BodyPositions Body;
	public Material MaterialBubble;
	public GameObject prefab;
	public GUISkin MySkin;
	public Texture2D m_directoryImage,m_fileImage;
	
	private Bubbles BubbleInput;
	
	private float DistanceError;
	private int CurrentCapture;
	private int CureentRepetition;
	private int Mod;
	private int NumberCaptrues;
	private int NumberPosition;
	private int Repetitions;
	private bool EndCaptures;
	private bool Init;
	private bool Cargado;
	private bool StartGame;
	private string FileName;
	
	// Use this for initialization
	void Awake () {
		Serializator = new SerialGame();
		Body = new BodyPositions();
		BubbleInput = new Bubbles();
		
		DistanceError=0.1f;
		CurrentCapture=1;
		CureentRepetition=0;
		Init=false;
		Cargado=false;
		StartGame=false;
		FileName="";
		GameObject.Find("Bubbles Particles").renderer.enabled=false;
		GameObject.Find("Bubbles Particles").audio.mute=true;
	}
	
	void Start(){		
	}
	
	// Update is called once per frame
	void Update () {
		if(Cargado){
			if(!StartGame){
				Body=Serializator.Load(FileName);
				Repetitions = Body.Repetitions;
				NumberCaptrues=Body.Captures;
				Mod = Body.Mode;
				BubbleInput.LoadBubble(Body);
				BubbleInput.InitBubbles(0.2f,Mod,MaterialBubble);
				LoadVideo(Body.VideoUrl);
				StartGame=true;
			}else{
				if(IsCalibrated()){
					if(Input.GetKeyDown(KeyCode.Home)){
						Init=true;
					}
					if(Init){
						if(CureentRepetition<Repetitions){
							BubbleInput.PutBubbles(CurrentCapture,Mod,Skel.Torso.transform.position);
							switch (Mod){
							case 1:
								if(BubbleInput.PositionComparator(DistanceError, Skel.LeftElbow.transform.position, Skel.LeftHand.transform.position)){
									//instanciamos las particulas para que parezca que explotan
									Instantiate(prefab, Skel.LeftHand.transform.position,Quaternion.identity);
									Instantiate(prefab, Skel.LeftElbow.transform.position,Quaternion.identity);
									CurrentCapture++;
									if(CurrentCapture==NumberCaptrues){
										CureentRepetition++;
									}
								}
								break;
							case 2:
								if(BubbleInput.PositionComparator(DistanceError, Skel.RightElbow.transform.position, Skel.RightHand.transform.position)){
									//instanciamos las particulas para que parezca que explotan
									Instantiate(prefab, Skel.RightHand.transform.position,Quaternion.identity);
									Instantiate(prefab, Skel.RightElbow.transform.position,Quaternion.identity);
									CurrentCapture++;
									if(CurrentCapture==NumberCaptrues){
										CureentRepetition++;
									}
								}
								break;
							case 3:
								break;
							case 4:
								break;
							case 5:
								if(BubbleInput.PositionComparator(DistanceError, Skel.LeftKnee.transform.position, Skel.LeftFoot.transform.position)){
									//instanciamos las particulas para que parezca que explotan
									Instantiate(prefab, Skel.LeftKnee.transform.position,Quaternion.identity);
									Instantiate(prefab, Skel.LeftFoot.transform.position,Quaternion.identity);
									CurrentCapture++;
									if(CurrentCapture==NumberCaptrues){
										CureentRepetition++;
									}
								}
								break;
							case 6:
								if(BubbleInput.PositionComparator(DistanceError, Skel.RightKnee.transform.position, Skel.RightFoot.transform.position)){
									//instanciamos las particulas para que parezca que explotan
									Instantiate(prefab, Skel.RightKnee.transform.position,Quaternion.identity);
									Instantiate(prefab, Skel.RightFoot.transform.position,Quaternion.identity);
									CurrentCapture++;
									if(CurrentCapture==NumberCaptrues){
										CureentRepetition++;
									}
								}
								break;
							case 7:
								break;
							}
						}else{
							UnityEngine.Debug.Log("Fin");
							AutoFade.LoadLevel(0,3,1,Color.white);
						}
					}
				}
			}
		}
	}
	
	void OnGUI(){
		if(!Cargado){			
			if(FileName==""){
				if (m_fileBrowser != null) {
					m_fileBrowser.OnGUI();
				} else {
					GUILayout.BeginArea(new Rect(Screen.width/2-140,Screen.height/2,200,100));
					GUI.skin=MySkin;
					if (GUILayout.Button("Abrir Ejercicio",  GUILayout.Width(280), GUILayout.Height(60))){
						m_fileBrowser = new FileBrowser(new Rect(100, 100, 600, 500),"Elije el video",FileSelectedCallback);
						m_fileBrowser.SelectionPattern= "*.myb";
						m_fileBrowser.DirectoryImage = m_directoryImage;
						m_fileBrowser.FileImage = m_fileImage;
					}
					GUI.skin=null;
					GUILayout.EndArea();
				}
			}else{
				Cargado=true;
			}
		}
		
	}
	
	public void LoadVideo(string VideoPath){
		ProcessStartInfo procinfo = new ProcessStartInfo();
		procinfo.WorkingDirectory= @"C:\Program Files (x86)\VideoLAN\VLC\"; 
		procinfo.FileName = "vlc.exe";
		procinfo.Arguments =@VideoPath;
		System.Diagnostics.Process.Start(procinfo);
		StartGame=true;
	}
	
	protected void FileSelectedCallback(string path) {
		m_fileBrowser = null;
		FileName = path;
	}
	
	public bool IsCalibrated(){
		if(Skel.Head.transform.position!=new Vector3(0f,1.945635f,0f)){
			return true;
		}else{
			return false;
		}
	}
	
	
}
