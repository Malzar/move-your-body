﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Bubbles : MonoBehaviour {
	
	public List<GameObject> BubblesObject;
	public List<Vector3> BubblesPosition;
	private bool Loaded= false;
	
	// Use this for initialization
	
	void Start () {
	
	}
	
	public void InitBubbles(float size, int mode, Material mat){
		if(Loaded){
			BubblesObject = new List<GameObject>();
			switch (mode){
			case 1: case 2: case 5: case 6:
				for (int i=0; i<=1; i++){
					GameObject Baux = GameObject.CreatePrimitive(PrimitiveType.Sphere);
					Baux.name="Bubble"+(i+1);
					Baux.transform.localScale= new Vector3(size, size, size);
					Baux.transform.position = new Vector3(0f,-1f,0f);
					Baux.renderer.material=mat;
					BubblesObject.Add(Baux);
				}
				break;
			case 3: case 7:
				for (int i=0; i<=3; i++){
					GameObject Baux = GameObject.CreatePrimitive(PrimitiveType.Sphere);
					Baux.name="Bubble"+(i+1);
					Baux.transform.localScale= new Vector3(size, size, size);
					Baux.transform.position = new Vector3(0f,-1f,0f);
					Baux.renderer.material=mat;
					BubblesObject.Add(Baux);
				}
				break;
			case 4:
				for (int i=0; i<=4; i++){
					GameObject Baux = GameObject.CreatePrimitive(PrimitiveType.Sphere);
					Baux.name="Bubble"+(i+1);
					Baux.transform.localScale= new Vector3(size, size, size);
					Baux.transform.position = new Vector3(0f,-1f,0f);
					Baux.renderer.material=mat;
					BubblesObject.Add(Baux);
				}
				break;
			case 8:
				for (int i=0; i<=7; i++){
					GameObject Baux = GameObject.CreatePrimitive(PrimitiveType.Sphere);
					Baux.name="Bubble"+(i+1);
					Baux.transform.localScale= new Vector3(size, size, size);
					Baux.transform.position = new Vector3(0f,-1f,0f);
					Baux.renderer.material=mat;
					BubblesObject.Add(Baux);
				}
				break;
			}
		}
		
	}
	
	public void LoadBubble(BodyPositions body){
		BubblesPosition = new List<Vector3>();
		
		switch (body.Mode){
		case 1: 
			for(int i=0; i<= body.Captures-1; i++){				
				BubblesPosition.Add(body.Elbow_Left[i]);
				BubblesPosition.Add(body.Hand_Left[i]);
			}
			break;
		case 2:
			for(int i=0; i<= body.Captures-1; i++){				
				BubblesPosition.Add(body.Elbow_Right[i]);
				BubblesPosition.Add(body.Hand_Right[i]);
			}
			break;
		case 3:
			for(int i=0; i<= body.Captures-1; i++){				
				BubblesPosition.Add(body.Elbow_Left[i]);
				BubblesPosition.Add(body.Hand_Left[i]);
				BubblesPosition.Add(body.Elbow_Right[i]);
				BubblesPosition.Add(body.Hand_Right[i]);
			}
			break;
		case 4:
			for(int i=0; i<= body.Captures-1; i++){
				BubblesPosition.Add(body.Torso[i]);
				BubblesPosition.Add(body.Elbow_Left[i]);
				BubblesPosition.Add(body.Hand_Left[i]);
				BubblesPosition.Add(body.Elbow_Right[i]);
				BubblesPosition.Add(body.Hand_Right[i]);
			}
			break;
		case 5:
			for(int i=0; i<= body.Captures-1; i++){				
				BubblesPosition.Add(body.Knee_Left[i]);
				BubblesPosition.Add(body.Foot_Left[i]);
			}
			break;
		case 6:
			for(int i=0; i<= body.Captures-1; i++){				
				BubblesPosition.Add(body.Knee_Right[i]);
				BubblesPosition.Add(body.Foot_Right[i]);
			}
			break;
		case 7:
			for(int i=0; i<= body.Captures-1; i++){				
				BubblesPosition.Add(body.Knee_Left[i]);
				BubblesPosition.Add(body.Foot_Left[i]);
				BubblesPosition.Add(body.Knee_Right[i]);
				BubblesPosition.Add(body.Foot_Right[i]);
			}
			break;
		case 8:
			for(int i=0; i<= body.Captures-1; i++){
				BubblesPosition.Add(body.Torso[i]);
				BubblesPosition.Add(body.Elbow_Left[i]);
				BubblesPosition.Add(body.Hand_Left[i]);
				BubblesPosition.Add(body.Elbow_Right[i]);
				BubblesPosition.Add(body.Hand_Right[i]);
				BubblesPosition.Add(body.Knee_Left[i]);
				BubblesPosition.Add(body.Foot_Left[i]);
				BubblesPosition.Add(body.Knee_Right[i]);
				BubblesPosition.Add(body.Foot_Right[i]);
			}
			break;			
		}
		Loaded=true;
	}
	
	public void PutBubbles(int capture, int mode, Vector3 referencePosition){
		if(Loaded){
			int aux =0;
			switch (mode){
			case 1: case 2: case 5: case 6:
				for(int i=0; i<=1;i++){
					aux=(capture*2)-2+i;
					BubblesObject[i].transform.position=(BubblesPosition[aux]+referencePosition);
				}
				break;
			case 3: case 7:
				for(int i=0; i<=3;i++){
					aux=(capture*4)-4+i;
					BubblesObject[i].transform.position=(BubblesPosition[aux]+referencePosition);
				}
				break;
			case 4:
				for(int i=0; i<=4;i++){
					aux=(capture*5)-5+i;
					BubblesObject[i].transform.position=(BubblesPosition[aux]+referencePosition);
				}
				break;
			case 8:
				for(int i=0; i<=7;i++){
					aux=(capture*8)-8+i;
					BubblesObject[i].transform.position=(BubblesPosition[aux]+referencePosition);
				}
				break;
			}
		}
		
	}
	
	public bool PositionComparator(float closeDistance, Vector3 internalPosition, Vector3 externalPosition){
		bool pos1Result =false;
		bool pos2Result =false;
		
		Vector3 subtraction= BubblesObject[0].transform.position-internalPosition;
		float sqrLen=subtraction.sqrMagnitude;
		if(sqrLen<closeDistance*closeDistance){
			pos1Result=true;
		}
		else{
			pos1Result=false;
		}
		
		subtraction= BubblesObject[1].transform.position-externalPosition;
		sqrLen=subtraction.sqrMagnitude;
		if(sqrLen<closeDistance*closeDistance){
			pos2Result=true;
		}
		else{
			pos2Result=false;
		}
		return (pos1Result&&pos2Result);
	}
	public bool PositionComparator(float closeDistance, Vector3 internalPositionLeft, Vector3 externalPositionLeft, Vector3 internalPositionRight, Vector3 externalPositionRight, Vector3 centerPosition){
	
		return true;
	}
}
