﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour {
	
	const int DELAY=60;
	public SerialGame Serializator;
	public ZigSkeleton Skel;
	public BodyPositions Body;
	public Material MaterialBubble;
	public GameObject prefab;
	
	private Bubbles BubbleInput;
	
	private float DistanceError;
	private int CurrentCapture;
	private int CureentRepetition;
	private int Mod;
	private int NumberCaptrues;
	private int NumberPosition;
	private int Repetitions;
	private bool EndCaptures;
	private bool Init;
	
	// Use this for initialization
	void Awake () {
		Serializator = new SerialGame();
		Body = new BodyPositions();
		BubbleInput = new Bubbles();
		
		DistanceError=0.1f;
		CurrentCapture=1;
		CureentRepetition=0;
		Init=false;
	}
	
	void Start(){
		GameObject.Find("Bubbles Particles").renderer.enabled=false;
		Body = Serializator.Load("prueba3.myb");
		Repetitions = Body.Repetitions;
		NumberCaptrues=Body.Captures;
		Mod = Body.Mode;
		BubbleInput.LoadBubble(Body);
		BubbleInput.InitBubbles(0.2f,Mod,MaterialBubble);
		
	}
	
	// Update is called once per frame
	void Update () {
		if(IsCalibrated()){
			if(Input.GetKeyDown(KeyCode.Home)){
				Init=true;
			}
			if(Init){
				if(CureentRepetition<Repetitions){
					BubbleInput.PutBubbles(CurrentCapture,Mod,Skel.Torso.transform.position);
					switch (Mod){
					case 1:
						if(BubbleInput.PositionComparator(DistanceError, Skel.LeftElbow.transform.position, Skel.LeftHand.transform.position)){
							//instanciamos las particulas para que parezca que explotan
							Instantiate(prefab, Skel.LeftHand.transform.position,Quaternion.identity);
							Instantiate(prefab, Skel.LeftElbow.transform.position,Quaternion.identity);
							CurrentCapture++;
							if(CurrentCapture==NumberCaptrues){
								CureentRepetition++;
							}
						}
						break;
					case 2:
						break;
					case 3:
						break;
					case 4:
						break;
					case 5:
						break;
					case 6:
						break;
					case 7:
						break;
					case 8:
						break;
					}
				}else{
					Debug.Log("Fin");
				}
			}
		}
		
	
	}
	
	public bool IsCalibrated(){
		if(Skel.Head.transform.position!=new Vector3(0f,1.945635f,0f)){
			return true;
		}else{
			return false;
		}
	}
	
	
}
