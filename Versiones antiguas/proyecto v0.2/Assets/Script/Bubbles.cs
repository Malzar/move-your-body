﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bubbles : MonoBehaviour {
	
	//constantes
	const int DELAY=60;
	public SerialGame Serializator;
	public ZigSkeleton Skel;
	public BodyPositions Body;
	public List<List<Vector3>> BubblesPosition; 
	
	public Material MaterialBubble;
	public GameObject[] BubblesObject;
	private bool[] PositionPass;
	private bool[] PositionPut;
	private List<Vector3> BubblesPositionOLD; //cambiar por un bubblePosition cuando e saquen los datos
	private float DistanceError=0.1f;
	private int num_pos=3;
	private int ActualPosition=0;	
	
	public int cont=0;
	public Vector3 _posAux;
	private bool AllPut=false;	
	private bool WinText=false;
	private bool ModeRec=true;
	public int _vecesguardas=0;
	
	//variables dde updates
	private int DelayRecUpdate=0;
	private bool SaveFile=false;
	private bool Calibrated=false;
	
	// Use this for initialization
	void Awake(){
		BubblesPosition = new List<List<Vector3>>();
		Body = new BodyPositions();
		Serializator = new SerialGame();
		
		
		BubblesPositionOLD = new List<Vector3>();
		//ocultar texto
		GameObject.Find("WinText").guiText.enabled=false;
		//iniciar las burbujas
		BubblesObject = new GameObject[num_pos];
		PositionPass = new bool[num_pos];
		PositionPut = new bool[num_pos+1];
		
		for(int i=0; i<num_pos;i++){
			
			GameObject Baux= GameObject.CreatePrimitive(PrimitiveType.Sphere);
			Baux.name="Burbuja "+(i+1);
			Baux.transform.localScale=new Vector3(0.2f,0.2f,0.2f);
			Baux.transform.position=new Vector3(0f,0f,0f);
			Baux.renderer.material=MaterialBubble;
			
			BubblesObject[i]=Baux;
			PositionPass[i]=false;
			PositionPut[i]=false;
		}
		PositionPut[0]=true;
		
		BubblesPositionOLD.Add(new Vector3(-0.5f,0.2f,-0.2f));
		BubblesPositionOLD.Add(new Vector3(-0.4f,-0.2f,-0.2f));
		BubblesPositionOLD.Add(new Vector3(-0.2f,0.6f,0.0f));
	}
	
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update(){
		if(!ModeRec){
			if(DelayRecUpdate==0){
				//Body=FileDat.Load("pruebadesalvado.myb");
				Debug.Log("me meto");
				if(Body!=null){
					foreach(Vector3 aux in Body.Hand_Left){
						Debug.Log(aux);
					}
				}
				DelayRecUpdate=DELAY;
			}
			DelayRecUpdate--;

		}else{
			//comprobar que este ya calibrado
			if(Calibrated==true){
				//comprobar si se requiere grabar
				if(!SaveFile){
					if(DelayRecUpdate==0){
						
						SaveSkeletonPosition(1);
						DelayRecUpdate=DELAY;
						_vecesguardas++;
											
						if(_vecesguardas>6){
							SaveFile=true;
						}
					}
					DelayRecUpdate--;
				}else{
					Serializator.Save(Body,"prueba2.myb");
					ModeRec=false;
					Body= Serializator.Load("prueba2.myb");
				}
			}else{
				Calibrated=IsCalibrated();
			}
		}
	}
	
	public bool IsCalibrated(){
		if(Skel.LeftKnee.transform.position!=new Vector3(0f,1.945635f,0f)){
			return true;
		}else{
			return false;
		}
	}

	public bool PositionComparator(Vector3 subtraction){
		float sqrLen=subtraction.sqrMagnitude;
		if(sqrLen<DistanceError*DistanceError){
			return true;
		}
		else{
			return false;
		}		
	}	
	
	public void PutBubbles(Vector3[] Bubble){
		
	}
	
	public void PutBubbles(){
		Vector3 posi=new Vector3();
		for(int i=0; i<(num_pos);i++){
			if(PositionPut[i]==true){
				posi=Skel.Torso.position+BubblesPositionOLD[i];
				BubblesObject[i].transform.position=posi;
			}else{
				i=num_pos;
			}
		}
	}
	
	public void SaveExercise(string fichName){
		Serializator.Save(Body, fichName);
	}
	
	public bool SaveSkeletonPosition(int mod){
		try{	
			Body.SaveSqueleton(mod,Skel);
			return true;
		}catch{
			return false;
		}
	}
}
