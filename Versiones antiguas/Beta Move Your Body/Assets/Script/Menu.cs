﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {
        
	public GUISkin MySkinMini;
	public GUISkin MySkin;
	public GUISkin MySkinHD;
	private int Width;
	private int Height;     
	private bool move;
	private int PxPlay, PyPlay;
	private int PxRecord, PyRecord;
	private int PxAbout, PyAbout;
	private int PxExit, PyExit;
	private int SpaceYButtons;
	private bool ToRecord,ToPlay,ToAbout,ToExit;
	public bool MvExit,MvAbout,MvRecord,MvPlay;

	
	
	void Awake(){
		if(GameObject.Find("ZigInputContainer")){
			Destroy(GameObject.Find("ZigInputContainer"));
		}
	}
	
    // Use this for initialization
    void Start () {
        move=false;
        MvPlay=false;
        MvRecord=false;
        MvAbout=false;
        MvExit=true;            
        PxPlay=PxRecord=PxAbout=PxExit=20;
    }
    
    // Update is called once per frame
    void Update () {
        Width=(int)(Screen.width/5.4);
        Height=(int)(Screen.height/9);
        SpaceYButtons=(int)(Screen.height/108);
        PyExit=Screen.height-(SpaceYButtons+Height);
        PyAbout=Screen.height-(Height*2+SpaceYButtons*2);
        PyRecord=Screen.height-(Height*3+SpaceYButtons*3);
        PyPlay=Screen.height-(Height*4+SpaceYButtons*4);
        
        if(move){                       
            if((MvExit)&&((PxExit+Width)>0)){
                    PxExit=MoveLeft(PxExit);
            }else{
                    MvExit=false;
            }
            if(PxExit+Width<Width/2){
                    MvAbout=true;
            }
            if((MvAbout)&&((PxAbout+Width)>0)){
                    PxAbout=MoveLeft(PxAbout);
            }else{
                    MvAbout=false;
            }
            if(PxAbout+Width<Width/2){
                    MvRecord=true;
            }
            if((MvRecord)&&((PxRecord+Width)>0)){
                    PxRecord=MoveLeft(PxRecord);
            }else{
                    MvRecord=false;
            }
            if(PxRecord+Width<Width/2){
            	MvPlay=true;
            }
            if((MvPlay)&&((PxPlay+Width)>0)){
                    PxPlay=MoveLeft(PxPlay);
            }else{
            	MvPlay=false;
            }
        }
		if((PxPlay+Width)<=0){
			if(ToPlay) AutoFade.LoadLevel(1,3,1,Color.white);
			if(ToRecord) AutoFade.LoadLevel(2,3,1,Color.white);
			if(ToAbout) AutoFade.LoadLevel(3,3,1,Color.white);
			if(ToExit) Application.Quit();
		}
    
    }
    
    private int MoveLeft(int Mx){
        return Mx-10;
    }
    
    void OnGUI(){
        if(Screen.width<1000){
            GUI.skin=MySkinMini;
        }else{
            if(Screen.width<1400){
                    GUI.skin=MySkin;
            }else{
                    GUI.skin=MySkinHD;
            }
        }
        
        if(GUI.Button(new Rect(PxPlay ,PyPlay ,Width,Height),"Jugar")){
            move=true;
            ToPlay=true;
        }
        if(GUI.Button(new Rect(PxRecord,PyRecord,Width,Height),"Grabar")){
            move=true;
            ToRecord=true;
        }
        if(GUI.Button(new Rect(PxAbout,PyAbout,Width,Height),"Acerca de")){
            move=true;
            ToAbout=true;
        }
        if(GUI.Button(new Rect(PxExit,PyExit,Width,Height),"Salir")){
            move=true;
            ToExit=true;
        }
            
    }
}
