﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BodyPositions : MonoBehaviour {
	
	[SerializeField]
	public int mode;
	public List<Vector3> Head;
	public List<Vector3> Neck;
	public List<Vector3> Torso;
	public List<Vector3> Waist;
	public List<Vector3> Collar_Left;
	public List<Vector3> Soulder_Left;
	public List<Vector3> Elbow_Left;
	public List<Vector3> Wrist_Left;
	public List<Vector3> Hand_Left;
	public List<Vector3> FingerTips_Left;
	public List<Vector3> Hip_Left;
	public List<Vector3> Knee_Left;
	public List<Vector3> Ankle_Left;
	public List<Vector3> Foot_Left;
	public List<Vector3> Collar_Right;
	public List<Vector3> Soulder_Right;
	public List<Vector3> Elbow_Right;
	public List<Vector3> Wrist_Right;
	public List<Vector3> Hand_Right;
	public List<Vector3> FingerTips_Right;
	public List<Vector3> Hip_Right;
	public List<Vector3> Knee_Right;
	public List<Vector3> Ankle_Right;
	public List<Vector3> Foot_Right;

	
	// Use this for initialization
	void Awake () {
		Head= new List<Vector3>();
		//Neck= new List<Vector3>();
		Torso= new List<Vector3>();
		//Waist= new List<Vector3>();
		//Collar_Left= new List<Vector3>();
		Soulder_Left= new List<Vector3>();
		Elbow_Left= new List<Vector3>();
		//Wrist_Left= new List<Vector3>();
		Hand_Left= new List<Vector3>();
		//FingerTips_Left= new List<Vector3>();
		Hip_Left= new List<Vector3>();
		Knee_Left= new List<Vector3>();
		//Ankle_Left= new List<Vector3>();
		Foot_Left= new List<Vector3>();
		//Collar_Right= new List<Vector3>();
		Soulder_Right= new List<Vector3>();
		Elbow_Right= new List<Vector3>();
		//Wrist_Right= new List<Vector3>();
		Hand_Right= new List<Vector3>();
		//FingerTips_Right= new List<Vector3>();
		Hip_Right= new List<Vector3>();
		Knee_Right= new List<Vector3>();
		//Ankle_Right= new List<Vector3>();
		Foot_Right= new List<Vector3>();
	}
	
	public void SaveSqueleton(int mode, ZigSkeleton Skel){
		Vector3 aux = Skel.Torso.transform.position;
		/* Mode 1: left arm
		 * Mode 2: right arm
		 * Mode 3: Arms
		 * Mode 4: up body
		 * Mode 5: left leg
		 * Mode 6: right leg
		 * Mode 7: down body
		 * Mode 8: complet body
		 */ 
		switch (mode){
		case 1:
			Head.Add(Skel.Head.transform.position-aux);
			Neck.Add(Skel.Neck.transform.position-aux);
			Torso.Add(Skel.Torso.transform.position-aux);
			Waist.Add(Skel.Waist.transform.position-aux);
			Collar_Left.Add(Skel.LeftCollar.transform.position-aux);
			Soulder_Left.Add(Skel.LeftShoulder.transform.position-aux);
			Elbow_Left.Add(Skel.LeftElbow.transform.position-aux);
			Wrist_Left.Add(Skel.LeftWrist.transform.position-aux);
			Hand_Left.Add(Skel.LeftHand.transform.position-aux);
			FingerTips_Left.Add(Skel.LeftFingertip.transform.position-aux);
			Hip_Left.Add(Skel.LeftHip.transform.position-aux);
			Knee_Left.Add(Skel.LeftKnee.transform.position-aux);
			Ankle_Left.Add(Skel.LeftAnkle.transform.position-aux);
			Foot_Left.Add(Skel.LeftFoot.transform.position-aux);
			Collar_Right.Add(Skel.RightCollar.transform.position-aux);
			Soulder_Right.Add(Skel.RightShoulder.transform.position-aux);
			Elbow_Right.Add(Skel.RightElbow.transform.position-aux);
			Wrist_Right.Add(Skel.RightWrist.transform.position-aux);
			Hand_Right.Add(Skel.RightHand.transform.position-aux);
			FingerTips_Right.Add(Skel.RightFingertip.position);
			Hip_Right.Add(Skel.RightHip.transform.position-aux);
			Knee_Right.Add(Skel.RightKnee.transform.position-aux);
			Ankle_Right.Add(Skel.RightAnkle.transform.position-aux);
			Foot_Right.Add(Skel.RightFoot.transform.position-aux);
			break;
		case 2:
			Head.Add(Skel.Head.transform.position-aux);
			Neck.Add(Skel.Neck.transform.position-aux);
			Torso.Add(Skel.Torso.transform.position-aux);
			Waist.Add(Skel.Waist.transform.position-aux);
			Collar_Left.Add(Skel.LeftCollar.transform.position-aux);
			Soulder_Left.Add(Skel.LeftShoulder.transform.position-aux);
			Elbow_Left.Add(Skel.LeftElbow.transform.position-aux);
			Wrist_Left.Add(Skel.LeftWrist.transform.position-aux);
			Hand_Left.Add(Skel.LeftHand.transform.position-aux);
			FingerTips_Left.Add(Skel.LeftFingertip.transform.position-aux);
			Hip_Left.Add(Skel.LeftHip.transform.position-aux);
			Knee_Left.Add(Skel.LeftKnee.transform.position-aux);
			Ankle_Left.Add(Skel.LeftAnkle.transform.position-aux);
			Foot_Left.Add(Skel.LeftFoot.transform.position-aux);
			Collar_Right.Add(Skel.RightCollar.transform.position-aux);
			Soulder_Right.Add(Skel.RightShoulder.transform.position-aux);
			Elbow_Right.Add(Skel.RightElbow.transform.position-aux);
			Wrist_Right.Add(Skel.RightWrist.transform.position-aux);
			Hand_Right.Add(Skel.RightHand.transform.position-aux);
			FingerTips_Right.Add(Skel.RightFingertip.position);
			Hip_Right.Add(Skel.RightHip.transform.position-aux);
			Knee_Right.Add(Skel.RightKnee.transform.position-aux);
			Ankle_Right.Add(Skel.RightAnkle.transform.position-aux);
			Foot_Right.Add(Skel.RightFoot.transform.position-aux);
			break;
		case 3:
			Head.Add(Skel.Head.transform.position-aux);
			Neck.Add(Skel.Neck.transform.position-aux);
			Torso.Add(Skel.Torso.transform.position-aux);
			Waist.Add(Skel.Waist.transform.position-aux);
			Collar_Left.Add(Skel.LeftCollar.transform.position-aux);
			Soulder_Left.Add(Skel.LeftShoulder.transform.position-aux);
			Elbow_Left.Add(Skel.LeftElbow.transform.position-aux);
			Wrist_Left.Add(Skel.LeftWrist.transform.position-aux);
			Hand_Left.Add(Skel.LeftHand.transform.position-aux);
			FingerTips_Left.Add(Skel.LeftFingertip.transform.position-aux);
			Hip_Left.Add(Skel.LeftHip.transform.position-aux);
			Knee_Left.Add(Skel.LeftKnee.transform.position-aux);
			Ankle_Left.Add(Skel.LeftAnkle.transform.position-aux);
			Foot_Left.Add(Skel.LeftFoot.transform.position-aux);
			Collar_Right.Add(Skel.RightCollar.transform.position-aux);
			Soulder_Right.Add(Skel.RightShoulder.transform.position-aux);
			Elbow_Right.Add(Skel.RightElbow.transform.position-aux);
			Wrist_Right.Add(Skel.RightWrist.transform.position-aux);
			Hand_Right.Add(Skel.RightHand.transform.position-aux);
			FingerTips_Right.Add(Skel.RightFingertip.position);
			Hip_Right.Add(Skel.RightHip.transform.position-aux);
			Knee_Right.Add(Skel.RightKnee.transform.position-aux);
			Ankle_Right.Add(Skel.RightAnkle.transform.position-aux);
			Foot_Right.Add(Skel.RightFoot.transform.position-aux);
			break;
		case 4:
			Head.Add(Skel.Head.transform.position-aux);
			Neck.Add(Skel.Neck.transform.position-aux);
			Torso.Add(Skel.Torso.transform.position-aux);
			Waist.Add(Skel.Waist.transform.position-aux);
			Collar_Left.Add(Skel.LeftCollar.transform.position-aux);
			Soulder_Left.Add(Skel.LeftShoulder.transform.position-aux);
			Elbow_Left.Add(Skel.LeftElbow.transform.position-aux);
			Wrist_Left.Add(Skel.LeftWrist.transform.position-aux);
			Hand_Left.Add(Skel.LeftHand.transform.position-aux);
			FingerTips_Left.Add(Skel.LeftFingertip.transform.position-aux);
			Hip_Left.Add(Skel.LeftHip.transform.position-aux);
			Knee_Left.Add(Skel.LeftKnee.transform.position-aux);
			Ankle_Left.Add(Skel.LeftAnkle.transform.position-aux);
			Foot_Left.Add(Skel.LeftFoot.transform.position-aux);
			Collar_Right.Add(Skel.RightCollar.transform.position-aux);
			Soulder_Right.Add(Skel.RightShoulder.transform.position-aux);
			Elbow_Right.Add(Skel.RightElbow.transform.position-aux);
			Wrist_Right.Add(Skel.RightWrist.transform.position-aux);
			Hand_Right.Add(Skel.RightHand.transform.position-aux);
			FingerTips_Right.Add(Skel.RightFingertip.position);
			Hip_Right.Add(Skel.RightHip.transform.position-aux);
			Knee_Right.Add(Skel.RightKnee.transform.position-aux);
			Ankle_Right.Add(Skel.RightAnkle.transform.position-aux);
			Foot_Right.Add(Skel.RightFoot.transform.position-aux);
			break;
		case 5:
			Head.Add(Skel.Head.transform.position-aux);
			Neck.Add(Skel.Neck.transform.position-aux);
			Torso.Add(Skel.Torso.transform.position-aux);
			Waist.Add(Skel.Waist.transform.position-aux);
			Collar_Left.Add(Skel.LeftCollar.transform.position-aux);
			Soulder_Left.Add(Skel.LeftShoulder.transform.position-aux);
			Elbow_Left.Add(Skel.LeftElbow.transform.position-aux);
			Wrist_Left.Add(Skel.LeftWrist.transform.position-aux);
			Hand_Left.Add(Skel.LeftHand.transform.position-aux);
			FingerTips_Left.Add(Skel.LeftFingertip.transform.position-aux);
			Hip_Left.Add(Skel.LeftHip.transform.position-aux);
			Knee_Left.Add(Skel.LeftKnee.transform.position-aux);
			Ankle_Left.Add(Skel.LeftAnkle.transform.position-aux);
			Foot_Left.Add(Skel.LeftFoot.transform.position-aux);
			Collar_Right.Add(Skel.RightCollar.transform.position-aux);
			Soulder_Right.Add(Skel.RightShoulder.transform.position-aux);
			Elbow_Right.Add(Skel.RightElbow.transform.position-aux);
			Wrist_Right.Add(Skel.RightWrist.transform.position-aux);
			Hand_Right.Add(Skel.RightHand.transform.position-aux);
			FingerTips_Right.Add(Skel.RightFingertip.position);
			Hip_Right.Add(Skel.RightHip.transform.position-aux);
			Knee_Right.Add(Skel.RightKnee.transform.position-aux);
			Ankle_Right.Add(Skel.RightAnkle.transform.position-aux);
			Foot_Right.Add(Skel.RightFoot.transform.position-aux);
			break;
		case 6:
			Head.Add(Skel.Head.transform.position-aux);
			Neck.Add(Skel.Neck.transform.position-aux);
			Torso.Add(Skel.Torso.transform.position-aux);
			Waist.Add(Skel.Waist.transform.position-aux);
			Collar_Left.Add(Skel.LeftCollar.transform.position-aux);
			Soulder_Left.Add(Skel.LeftShoulder.transform.position-aux);
			Elbow_Left.Add(Skel.LeftElbow.transform.position-aux);
			Wrist_Left.Add(Skel.LeftWrist.transform.position-aux);
			Hand_Left.Add(Skel.LeftHand.transform.position-aux);
			FingerTips_Left.Add(Skel.LeftFingertip.transform.position-aux);
			Hip_Left.Add(Skel.LeftHip.transform.position-aux);
			Knee_Left.Add(Skel.LeftKnee.transform.position-aux);
			Ankle_Left.Add(Skel.LeftAnkle.transform.position-aux);
			Foot_Left.Add(Skel.LeftFoot.transform.position-aux);
			Collar_Right.Add(Skel.RightCollar.transform.position-aux);
			Soulder_Right.Add(Skel.RightShoulder.transform.position-aux);
			Elbow_Right.Add(Skel.RightElbow.transform.position-aux);
			Wrist_Right.Add(Skel.RightWrist.transform.position-aux);
			Hand_Right.Add(Skel.RightHand.transform.position-aux);
			FingerTips_Right.Add(Skel.RightFingertip.position);
			Hip_Right.Add(Skel.RightHip.transform.position-aux);
			Knee_Right.Add(Skel.RightKnee.transform.position-aux);
			Ankle_Right.Add(Skel.RightAnkle.transform.position-aux);
			Foot_Right.Add(Skel.RightFoot.transform.position-aux);
			break;
		case 7:
			Head.Add(Skel.Head.transform.position-aux);
			Neck.Add(Skel.Neck.transform.position-aux);
			Torso.Add(Skel.Torso.transform.position-aux);
			Waist.Add(Skel.Waist.transform.position-aux);
			Collar_Left.Add(Skel.LeftCollar.transform.position-aux);
			Soulder_Left.Add(Skel.LeftShoulder.transform.position-aux);
			Elbow_Left.Add(Skel.LeftElbow.transform.position-aux);
			Wrist_Left.Add(Skel.LeftWrist.transform.position-aux);
			Hand_Left.Add(Skel.LeftHand.transform.position-aux);
			FingerTips_Left.Add(Skel.LeftFingertip.transform.position-aux);
			Hip_Left.Add(Skel.LeftHip.transform.position-aux);
			Knee_Left.Add(Skel.LeftKnee.transform.position-aux);
			Ankle_Left.Add(Skel.LeftAnkle.transform.position-aux);
			Foot_Left.Add(Skel.LeftFoot.transform.position-aux);
			Collar_Right.Add(Skel.RightCollar.transform.position-aux);
			Soulder_Right.Add(Skel.RightShoulder.transform.position-aux);
			Elbow_Right.Add(Skel.RightElbow.transform.position-aux);
			Wrist_Right.Add(Skel.RightWrist.transform.position-aux);
			Hand_Right.Add(Skel.RightHand.transform.position-aux);
			FingerTips_Right.Add(Skel.RightFingertip.position);
			Hip_Right.Add(Skel.RightHip.transform.position-aux);
			Knee_Right.Add(Skel.RightKnee.transform.position-aux);
			Ankle_Right.Add(Skel.RightAnkle.transform.position-aux);
			Foot_Right.Add(Skel.RightFoot.transform.position-aux);
			break;
		case 8:
			Head.Add(Skel.Head.transform.position-aux);
			Neck.Add(Skel.Neck.transform.position-aux);
			Torso.Add(Skel.Torso.transform.position-aux);
			Waist.Add(Skel.Waist.transform.position-aux);
			Collar_Left.Add(Skel.LeftCollar.transform.position-aux);
			Soulder_Left.Add(Skel.LeftShoulder.transform.position-aux);
			Elbow_Left.Add(Skel.LeftElbow.transform.position-aux);
			Wrist_Left.Add(Skel.LeftWrist.transform.position-aux);
			Hand_Left.Add(Skel.LeftHand.transform.position-aux);
			FingerTips_Left.Add(Skel.LeftFingertip.transform.position-aux);
			Hip_Left.Add(Skel.LeftHip.transform.position-aux);
			Knee_Left.Add(Skel.LeftKnee.transform.position-aux);
			Ankle_Left.Add(Skel.LeftAnkle.transform.position-aux);
			Foot_Left.Add(Skel.LeftFoot.transform.position-aux);
			Collar_Right.Add(Skel.RightCollar.transform.position-aux);
			Soulder_Right.Add(Skel.RightShoulder.transform.position-aux);
			Elbow_Right.Add(Skel.RightElbow.transform.position-aux);
			Wrist_Right.Add(Skel.RightWrist.transform.position-aux);
			Hand_Right.Add(Skel.RightHand.transform.position-aux);
			FingerTips_Right.Add(Skel.RightFingertip.position);
			Hip_Right.Add(Skel.RightHip.transform.position-aux);
			Knee_Right.Add(Skel.RightKnee.transform.position-aux);
			Ankle_Right.Add(Skel.RightAnkle.transform.position-aux);
			Foot_Right.Add(Skel.RightFoot.transform.position-aux);
			break;
		default:
			break;
			
		}
	}
	
}
