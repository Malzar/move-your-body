﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour {
	
	const int DELAY=60;
	public SerialGame Serializator;
	public ZigSkeleton Skel;
	public BodyPositions Body;
	public Material MaterialBubble;
	
	private Bubbles BubbleInput;
	
	private float DistanceError;
	private int NumberPosition;
	private int Repetitions;
	private int ActualPosition;	
	private bool AllPut;
	private int Mod;
	
	// Use this for initialization
	void Awake () {
		Serializator = new SerialGame();
		Body = new BodyPositions();
		BubbleInput = new Bubbles();
		
		float DistanceError=0.1f;
		int ActualPosition=0;	
		AllPut=false;
	}
	
	void Start(){
		Body = Serializator.Load("prueba3.myb");
		Repetitions = Body.Repetitions;
		Mod = Body.Mode;
		BubbleInput.LoadBubble(Body);
		BubbleInput.InitBubbles(0.2f,Mod,MaterialBubble);
		
	}
	
	// Update is called once per frame
	void Update () {
		BubbleInput.PutBubbles(2,Mod,Skel.Torso.transform.position);
	
	}
	
	public bool IsCalibrated(){
		if(Skel.LeftKnee.transform.position!=new Vector3(0f,1.945635f,0f)){
			return true;
		}else{
			return false;
		}
	}
	
	
}
