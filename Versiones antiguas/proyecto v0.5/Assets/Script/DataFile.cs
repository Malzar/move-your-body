﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class DataFile : MonoBehaviour {
	
	private FileStream fich;
	private BinaryFormatter Serializator;
	
	// Use this for initialization
	void awake () {

	}
	
	public bool SaveFile(string fileName, BodyPositions body){
		try{
			
			Debug.Log("mano"+body.Hand_Left[0]);
			Serializator = new BinaryFormatter();
			string FilePath = Directory.GetCurrentDirectory()+"\\"+fileName+".myb";
			fich = new FileStream(FilePath, FileMode.Create, FileAccess.Write,FileShare.Read);
			Serializator.Serialize(fich,body);
			fich.Close();
			return true;
		}catch{
			return false;
		}
	}
	
	public BodyPositions Load(string path){
		try{
			Serializator = new BinaryFormatter();
			BodyPositions aux;
			fich = new FileStream(path, FileMode.Open,FileAccess.Read);			
			aux=(BodyPositions)Serializator.Deserialize(fich);
			fich.Close();
			return aux;
		}catch{
			return null;
		}
		
	}
}
