﻿using UnityEngine;
using System.Collections;

public class MotionCapture : MonoBehaviour {
	
	const int DELAY=30;
	public SerialGame Serializator;
	public ZigSkeleton Skel;
	public BodyPositions Body;
	
	public bool Calibrated;
	public bool StartCapture;
	public int DelayRecord;
	
	public int Mode;//cambiarla por una entrada de campo
	
	void Awake(){
		Serializator = new SerialGame();
		Body = new BodyPositions();
		
		Calibrated =false;
		StartCapture =false;
		DelayRecord =0;
		Mode=1;
	}

	// Use this for initialization
	void Start () {
		GameObject.Find("Float Text").guiText.text = "Calibrando su cuerpo";
	}
	
	// Update is called once per frame
	void Update () {
		//comprobar la calibracion
		if(Calibrated){
			if(Input.GetKeyDown(KeyCode.Home)){
				StartCapture=true;
				GameObject.Find("Float Text").guiText.text="Grabando movimientos";
				GameObject.Find("Float Text").guiText.enabled=true;
			}
			
			if(StartCapture){
				if(DelayRecord==0){
					SaveSkeletonPosition(Mode);
					DelayRecord=DELAY;
				}
				DelayRecord--;
			}
			
			if((Input.GetKeyDown(KeyCode.Escape)==true)||(Input.GetKeyDown(KeyCode.End)==true)){
				StartCapture=false;
				SaveExercise("prueba3.myb");
				GameObject.Find("Float Text").guiText.text="Su fichero ha sido generado";
			}
			//a la espera de reconocimiento de voz
		}else{
			Calibrated = IsCalibrated();
		}
	}
	

	
	public bool IsCalibrated(){
		if(Skel.LeftKnee.transform.position!=new Vector3(0f,1.945635f,0f)){
			GameObject.Find("Float Text").guiText.enabled=false;
			return true;
		}else{
			return false;
		}
	}
	
	public void SaveExercise(string fichName){
		Serializator.Save(Body, fichName);
	}
	
	public bool SaveSkeletonPosition(int mod){
		try{	
			Body.SaveSqueleton(mod,Skel);
			return true;
		}catch{
			return false;
		}
	}
}
